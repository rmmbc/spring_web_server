package com.atos.rmmbc.faq_atos.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class FaqExceptions extends RuntimeException {
	
	public FaqExceptions(String message) {
		super(message);
	}

	public FaqExceptions(String message, Throwable cause) {
		super(message,cause);
	}
}
