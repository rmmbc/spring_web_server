package com.atos.rmmbc.faq_atos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.atos.rmmbc.faq_atos.entities.Faq;
@Repository
public interface FaqRepository extends JpaRepository<Faq,Long> {

}
