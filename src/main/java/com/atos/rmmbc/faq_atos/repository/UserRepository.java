package com.atos.rmmbc.faq_atos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.atos.rmmbc.faq_atos.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
