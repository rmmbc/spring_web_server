package com.atos.rmmbc.faq_atos.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.atos.rmmbc.faq_atos.dto.CategorieDTO;
import com.atos.rmmbc.faq_atos.entities.Categorie;
import com.atos.rmmbc.faq_atos.entities.Utils;
import com.atos.rmmbc.faq_atos.repository.CategorieRepository;

@RestController
public class CategorieController {

	@Autowired
	private CategorieRepository catRepo;

	@GetMapping(value = "/faq/categorie")
	public List<Categorie> get() {
		return catRepo.findAll();
	}

	@GetMapping(value = "/faq/categorie/{id}")
	public Categorie getCategorieById(@PathVariable long id) {
		Optional<Categorie> tmp = catRepo.findById(id);
		
		if (tmp.isPresent()) {
			System.out.println();
			return tmp.get();
		}
		return null;
	}

	@PostMapping(value = "/faq/categorie")
	public Object createCategorie(@RequestBody Categorie cat) {
		HashMap<String, Object> fields = new HashMap<String, Object>();

		fields.put("categorie", cat.getNomCategorie());

		if (Utils.validate(fields)) {
			List<Categorie> list = catRepo.findAll();

			for (Categorie u : list) {
				if (u.getNomCategorie().toLowerCase().equals(cat.getNomCategorie().toLowerCase())) {
					return null;
				}
			}
			catRepo.save(cat);
			return cat;
		}

		return Utils.getErrors();
	}
	
	@PutMapping(value="/faq/categorie")
	public Object update(@RequestBody CategorieDTO cat) {
		HashMap<String, Object> fields = new HashMap<String, Object>();
		
		fields.put("categorie", cat.getNomCategorie());
		
		Optional<Categorie> c = catRepo.findById(cat.getIdCategorie());
		if(Utils.validate(fields)){
			if(c.isPresent()) {
				Categorie categorie = c.get();
				categorie.setNomCategorie(cat.getNomCategorie());
				catRepo.save(categorie);
				return cat;
			}else {
				return new String("Cannot update unexistant entity");
			}
		}
		return Utils.getErrors();
		
	}

	@DeleteMapping(value="/faq/categorie/{id}")
	public Object delete(@PathVariable long id) {
		try {
			catRepo.deleteById(id);
			return 200;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new String("categorie id doesn't exist");
		}
		
	}
}
