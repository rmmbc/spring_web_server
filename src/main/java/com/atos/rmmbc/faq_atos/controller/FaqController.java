package com.atos.rmmbc.faq_atos.controller;

import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.atos.rmmbc.faq_atos.dto.FaqDTO;
import com.atos.rmmbc.faq_atos.entities.Categorie;
import com.atos.rmmbc.faq_atos.entities.Faq;
import com.atos.rmmbc.faq_atos.entities.User;
import com.atos.rmmbc.faq_atos.entities.Utils;
import com.atos.rmmbc.faq_atos.repository.FaqRepository;

@RestController
public class FaqController {

	@Autowired
	private FaqRepository faqRepo;
	@Autowired
	private UserController userController;
	@Autowired
	private CategorieController catController;

	@GetMapping(value = "/faq/faqs/")
	public List<Faq> get() {
		return faqRepo.findAll();
	}

	@GetMapping(value = "/faq/faqs/{id}")
	public Object getFaqById(@PathVariable long id) {
		try {
			Optional<Faq> f = faqRepo.findById(id);
			System.out.println(f.isPresent());
			return f.get();
			
		}catch(NoSuchElementException n){
			System.out.println(n.getMessage());
			return -1;
		}
		catch (IllegalArgumentException i) {
			System.out.println(i.getMessage());
			return -1;
		}
	}

	@PostMapping(value = "/faq/faqs/")
	public Object addFaq(@RequestBody FaqDTO faq) {
		HashMap<String, Object> fields = new HashMap<String, Object>();
		fields.put("question", faq.getQuestion());
		fields.put("reponse", faq.getReponse());
		fields.put("date", faq.getDate());
		fields.put("idCategorie", faq.getIdCategorie());
		fields.put("idUser", faq.getIdUser());
		if (Utils.validate(fields)) {
			User u = userController.getUser((long) faq.getIdUser());
			Categorie c = catController.getCategorieById((long) faq.getIdCategorie());
			if (u != null && c != null) {
				Faq f = new Faq();
				f.setCategorie(c);
				f.setUser(u);
				f.setQuestion(faq.getQuestion());
				f.setReponse(faq.getReponse());
				faqRepo.save(f);
				return faq;
			}
			return fields;
		}
		return Utils.getErrors();
	}

	@DeleteMapping(value="/faq/faqs/{id}")
	public int deleteFaq(@PathVariable long id) {
		try {
			faqRepo.deleteById(id);
			return 200;
		}catch(IllegalArgumentException i) {
			System.out.println(i.getMessage());
			return -1;
		}
	}
	
	@PutMapping(value="/faq/faqs/")
	public Object updateFaq(@RequestBody FaqDTO faqDTO) {
		try {
			Optional<Faq> f = faqRepo.findById((long)faqDTO.getId());
			System.out.println("id faq" +faqDTO.getId());
			if(f.isPresent()) {
				Faq faq = f.get();
				faq.setQuestion(faqDTO.getQuestion());
				faq.setReponse(faqDTO.getReponse());
				Categorie c = catController.getCategorieById(faqDTO.getIdCategorie());
				if(c!=null) {
					faq.setCategorie(c);
					faqRepo.save(faq);
					return faq;	
				}
				
			}
			System.out.println("not present");
			return -1;
			
		}catch(IllegalArgumentException i) {
			System.out.println(i.getMessage());
			return -1;
		}
	}
	
}
