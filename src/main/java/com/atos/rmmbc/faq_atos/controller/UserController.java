package com.atos.rmmbc.faq_atos.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import com.atos.rmmbc.faq_atos.dto.UserDTO;
import com.atos.rmmbc.faq_atos.entities.User;
import com.atos.rmmbc.faq_atos.entities.Utils;
import com.atos.rmmbc.faq_atos.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@Autowired
	private UserRepository userRepository;

	@GetMapping(value = "/faq/user")
	public List<User> getUser() {
		Object u = 1;
		if (Integer.class.isInstance(u)) {
			System.out.println(u);

		}

		return userRepository.findAll();
	}

	@GetMapping(value = "/faq/user/{id}")
	public User getUser(@PathVariable Long id) {
		Optional<User> optionalUser = userRepository.findById(id);
		if (optionalUser.isPresent()) {
			return optionalUser.get();
		}
		return null;
	}

	@PostMapping(value = "/faq/user")
	public Object addClient(@RequestBody UserDTO userDto) {
		User user = new User();
		user.setNom(userDto.getNom());
		user.setPrenom(userDto.getPrenom());
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());
		user.setTelephone(userDto.getTelephone());
		HashMap<String, Object> fields = new HashMap<String, Object>();

		fields.put("nom", userDto.getNom());
		fields.put("prenom", userDto.getPrenom());
		fields.put("email", userDto.getEmail());
		fields.put("telephone", userDto.getTelephone());
		fields.put("password", userDto.getNom());

		if (Utils.validate(fields)) {
			List<User> list = userRepository.findAll();

			for (User u : list) {
				if (u.getEmail().equals(user.getEmail())) {
					return null;
				}
			}
			userRepository.save(user);
			return user;
		}

		return Utils.getErrors();

	}

	@PutMapping(value = "/faq/user/")
	public User updateUser(@RequestBody User user) {
		Optional<User> optionalUser = userRepository.findById(user.getId());
		if (optionalUser.isPresent()) {
			User u = optionalUser.get();
			u.setNom(user.getNom());
			u.setPrenom(user.getPrenom());
			u.setEmail(user.getEmail());
			u.setPassword(user.getPassword());
			u.setTelephone(user.getPrenom());
			userRepository.save(u);
			return user;
		}
		return null;
	}

	@DeleteMapping(value = "/faq/user/{id}")
	public int delete(@PathVariable Long id) {
		List<User> list = userRepository.findAll();
		for (User u : list) {
			if (u.getId() == id) {
				userRepository.deleteById(id);
				return 200;
			}
		}
		return -1;
	}
}
