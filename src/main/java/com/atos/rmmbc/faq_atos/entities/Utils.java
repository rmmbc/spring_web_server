package com.atos.rmmbc.faq_atos.entities;

import java.util.Date;
import java.util.HashMap;

public class Utils {
	public static HashMap<String, String> errors = new HashMap<String, String>();

	public static boolean validate(HashMap<String, Object> fields) {
		for (HashMap.Entry<String, Object> entry : fields.entrySet()) {

			String key = entry.getKey();
			Object _value = entry.getValue();
			
			if (String.class.isInstance(_value)) {
				String value = (String) _value;
				if (value.length() != 0) {
					if (key.equals("email") && !value.contains("@")) {	
						errors.put(key, " email format is not valid ");
					}
					if (key.equals("telephone") && value.length() < 12) {
						errors.put(key, " the value of telphone is not correct");
					}
				}else {
					errors.put(key,key+" mustn't be empty");
				}

			}
			if (Integer.class.isInstance(_value)) {
	
				if ((int) _value <= 0) {
					errors.put(key, " mustn't be negative or null");
				}

			}
			if(key.equals("date")) {
				
				if(_value==null) {
					errors.put(key, "date musn't be null");
				}
			}

		}
		return errors.size() != 0 ? false : true;
	}

	public static HashMap<String, String> getErrors() {
		return errors;
	}
}
