package com.atos.rmmbc.faq_atos;


import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.atos.rmmbc.faq_atos.controller.UserController;
import com.atos.rmmbc.faq_atos.dto.UserDTO;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserTest {
	private UserDTO user = new UserDTO();

	@Autowired
	private UserController userController ;
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void addClient() {
		user.setEmail("sophie@gmail.com");
		user.setNom("Ndiaye");
		user.setPrenom("Sophie");
		assertNull(userController.addClient(user));
	}
}
